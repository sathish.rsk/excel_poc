import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import 'hammerjs';
import {MatSnackBar} from '@angular/material';
export interface IRunmode {
    value: string;
    viewValue: string;
}
export interface IKeywords {
    value: string;
    viewValue: string;
}
export interface Fruit {
    name: string;
}
interface Isheet {
    sheetName: string,
    sheetColumns: Object[],
    sheetData: Object[]
}
interface Idata {
    name: string,
    list: any[]
}
interface Itcid {
    TCID: string,
    Description: string,
    Keyword: string,
    Object: string,
    ObjectType: string,
    Data: string,
    ProceedOnFail: string
}
interface IkeyWord {
    arr: Itcid
}
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    datas = "";
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    dynamicForm: FormGroup;
    dynamicFormArr: IkeyWord[] = [];
    isOptional = false;
    keyWordSheetData: IkeyWord[] = [];
    filterData: object = [];
    testData: Idata[] = [];
    public Runmodes = [
        { value: 'Y', viewValue: 'Yes' },
        { value: 'N', viewValue: 'No' },
    ];
    panelOpenState = false;
    constructor(
        private _http: HttpClient,
        private _formBuilder: FormBuilder,
        public _snackBar: MatSnackBar
    ) { }

    public Keywords = [
        { value: 'GOTOURL', viewValue: 'GOTOURL' },
        { value: 'READ_DATA', viewValue: 'READ_DATA' },
        { value: 'HOVER', viewValue: 'HOVER' }
    ];
    resetData(val) {
        if (val == 1) {
            this.firstFormGroup.reset();
        } else {
            this.secondFormGroup.reset();
            this.secondFormGroup.get('ProceedOnFail').setValue('Y');
        }
    }
    filterCus(itm) {
        this.filterData = this.filtersideLane(itm.name);
    }
    filtersideLane(itm) {
        return this.keyWordSheetData.filter(record => {
            return itm == record.arr.TCID;
        });
    }
    filterForm() {
        if (this.thirdFormGroup.valid) {
            let _fb = this._formBuilder.group({});
            this.dynamicFormArr = this.filtersideLane(this.thirdFormGroup.value.TCID);
            this.dynamicFormArr = this.dynamicFormArr.filter(rec => {
               return ((rec.arr.Data != "") && (rec.arr.Data))
            });
            this.dynamicFormArr.forEach(rec => {
                _fb.addControl(rec.arr.Data, this._formBuilder.control('', Validators.required));
            });
            this.dynamicForm = _fb;
        }
        if (!this.panelOpenState) {
            this.panelOpenState = true;
            let x: string;
            this.TCID.forEach(rec => {
                x = rec.name;
                let temObj: Idata = {
                    name: x,
                    list: []
                };
                this.testData.push(temObj);
            });
            console.log(this.testData);
        }
    }
    testMethd() {

    }
    ngOnInit() {
        this.testMethd();
        this.firstFormGroup = this._formBuilder.group({
            TestSuite: ['', Validators.required],
            Iteration: ['', Validators.required],
            Runmode: ['Y', Validators.required],
            TCID: ['', Validators.required],
        });
        this.secondFormGroup = this._formBuilder.group({
            TCID: ['', Validators.required],
            Description: ['', Validators.required],
            Keyword: ['', Validators.required],
            Object: ['', Validators.required],
            ObjectType: ['', Validators.required],
            Data: [''],
            ProceedOnFail: ['Y', Validators.required],
        });
        this.thirdFormGroup = this._formBuilder.group({
            TCID: ['', Validators.required],
        });
        this.dynamicForm = this._formBuilder.group({
            some: ['', Validators.required]
        });
    }
    getsheetstructure(): Isheet {
        return {
            sheetName: "",
            sheetColumns: [],
            sheetData: []
        }
    }
    createTestSuite(sheets) {
        let sheet: Isheet = this.getsheetstructure(), columkey = this.firstFormGroup.value, sdata = [];
        sheet.sheetName = "TestSuite";
        for (const key in columkey) {
            let col = {
                "header": key,
                "key": key,
                "width": 30
            };
            if (key != "TCID") {
                sheet.sheetColumns.push(col);
                sdata.push(columkey[key]);
            }
        }
        sheet.sheetData = [{
            "arr": sdata
        }];
        sheets.push(sheet);
    }
    createTestCaseSheet(sheets) {
        let sheet: Isheet = this.getsheetstructure(), columkey = this.firstFormGroup.value;
        sheet.sheetName = "TestCases";
        sheet.sheetColumns = [
            { "header": "TestSuite", "key": "TestSuite", "width": 30 },
            { "header": "TCID", "key": "TCID", "width": 30 }
        ]
        columkey.TCID.forEach(element => {
            sheet.sheetData.push(
                { "arr": [columkey.TestSuite, element.name] }
            );
        });
        sheets.push(sheet);
    }
    createKeywordSheet(sheets) {
        let sheet: Isheet = this.getsheetstructure(), columkey = this.secondFormGroup.value, sdata = [];
        sheet.sheetName = "Keyword";
        for (const key in columkey) {
            let col = {
                "header": key,
                "key": key,
                "width": 30
            };
            sheet.sheetColumns.push(col);
            sdata.push(columkey[key]);
        }
        sheet.sheetData = this.keyWordSheetData;
        //this.keyWordSheetData = [];
        sheets.push(sheet);
    }
    createDataSheet(sheets) {
        let sheet: Isheet = this.getsheetstructure(), sdata = [];
        sheet.sheetName = "Data";
        this.testData.forEach(rec => {
            let ctrl = true;
            if (rec.list.length > 0) {
                sdata.push({
                    color: "ffa500",
                    arr: [this.firstFormGroup.value.TestSuite, rec.name]
                });
                rec.list.forEach((record, index) => {
                    let templArr = ["Runmode", "Iteration"];
                    let tArr = ["Y", "#" + (index + 1)];
                    for (const key in record) {
                        tArr.push(record[key]);
                        if (ctrl) {
                            templArr.push(key);
                        }
                    }
                    if (ctrl) {
                        sdata.push({
                            color: "ffff00",
                            arr: templArr
                        });
                        ctrl = false;
                    }
                    sdata.push({
                        arr: tArr
                    });
                });
                sdata.push({
                    arr: []
                });
            }
        });
        console.log(sdata);
        sheet.sheetData = sdata;
        sheets.push(sheet);
    }
    postFUnc() {
        let sheets: Isheet[] = [];
        this.createTestSuite(sheets);
        this.createTestCaseSheet(sheets);
        this.createKeywordSheet(sheets);
        this.createDataSheet(sheets);
        console.log(sheets);
        let xlsx = {
            "filename": "testCase",
            "sheets": sheets
        }
        console.log(xlsx);
        this._http.post("http://localhost:9090/api/excel/create/testcases", xlsx).subscribe(res => {
            console.log(res)
        });
    }
    submitData(val) {
        if (val == 1) {
            this.firstFormGroup.get('TCID').setValue(this.TCID);
            this.datas = this.firstFormGroup.value.TestSuite;
            console.log(this.firstFormGroup.value);
            this._snackBar.open("Your response is recorded", "Click Next", {
                duration: 2000,
            });
        } else {
            if (val == 2) {
                this.keyWordSheetData.push({ "arr": this.secondFormGroup.value });
                console.log(this.secondFormGroup.value);
                this._snackBar.open("Your response is recorded", "Click Next", {
                    duration: 2000,
                });
            } else {
                this.testData.map(rec => {
                    if (rec.name == this.thirdFormGroup.value.TCID) {
                        rec.list.push(this.dynamicForm.value);
                    }
                });
                this._snackBar.open("Your response is recorded", "Click Next", {
                    duration: 2000,
                });
            }
        }
    }

    //UnTouchable
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    TCID = [
        { name: 'Load Webpage' },
        { name: 'Read Page Desc' },
    ];
    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;
        if ((value || '').trim()) {
            this.TCID.push({ name: value.trim() });
        }
        if (input) {
            input.value = '';
        }
    }
    remove(fruit: Fruit): void {
        const index = this.TCID.indexOf(fruit);
        if (index >= 0) {
            this.TCID.splice(index, 1);
        }
    }
}
//C:\> "C:\Program Files\MySQL\MySQL Server 8.0\bin\mysqld" --install