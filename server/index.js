const express = require("express");
const bodyParser = require('body-parser');
const path = require('path');
const Excel = require('exceljs');

const myApp = express();


myApp.use(bodyParser.json({limit: '10mb'}));

myApp.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET, PUT, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

const port = 9090;

myApp.listen(port , () => console.log("server started on port   "+port));

myApp.get('/' , (req, res) => {
    res.send("hello");
})

function SetWorkbookProperties(workbook){
    workbook.creator = 'sathish';
    workbook.lastModifiedBy = 'sathish';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.properties.date1904 = true;
    workbook.views = [
      {
        x: 0, y: 0, width: 10000, height: 20000,
        firstSheet: 0, activeTab: 1, visibility: 'visible'
      }
    ]
}

function addSheetsTOWorkBook(data , workbook){
    data.sheets.forEach(record => {
        var sheetRef  = workbook.addWorksheet(record.sheetName);
        if(record.sheetColumns && record.sheetColumns.length > 0){
            sheetRef.columns = record.sheetColumns;
            sheetRef.getRow(1).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFF00'
                }
            };
        }
    })
}

function createAnXlsx(data , workbook){
    return workbook.xlsx.writeFile('./' + data.filename + '.xlsx')
}

function appendDataToSheet(data , workbook){
    var sheet = workbook.getWorksheet(data.sheetName);
    data.sheetData.forEach(rec => {
        //console.log(rec);
        sheet.addRow(rec.arr);
        if(rec.color && rec.color != "FFFFFFFF"){
            var row = sheet.lastRow;
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: rec.color
                }
            };
        }
    });
}

myApp.post('/api/excel/create/testcases' , (req , res) => {
    
    let workbook = new Excel.Workbook();
    SetWorkbookProperties(workbook);
    addSheetsTOWorkBook(req.body , workbook);
    req.body.sheets.forEach(record  => {
        appendDataToSheet(record , workbook);
    });
    res.send(createAnXlsx(req.body , workbook));
    
});
myApp.post('/api/excel/create/appendTOXlsx' , (req , res) => {
    
    let workbook = new Excel.Workbook();
    SetWorkbookProperties(workbook);
    let filecus = './' + req.body.filename+'.xlsx';
    workbook.xlsx.readFile(filecus).then((succ , err) => {
        if(err){
            res.send(err.message);
        }
        req.body.sheets.forEach(record  => {
            appendDataToSheet(record , workbook);
        });
        res.send(createAnXlsx(req.body , workbook));
    });
});